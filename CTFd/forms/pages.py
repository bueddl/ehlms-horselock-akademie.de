from wtforms import (
    BooleanField,
    HiddenField,
    MultipleFileField,
    StringField,
    TextAreaField,
    IntegerField,
    SelectField
)
from wtforms.validators import InputRequired

from CTFd.forms import BaseForm
from CTFd.models import Pages


class PageEditForm(BaseForm):
    title = StringField(
        "Title", description="This is the title shown on the navigation bar"
    )
    route = StringField(
        "Route",
        description="This is the URL route that your page will be at (e.g. /page). You can also enter links to link to that page.",
    )
    parent_page_id = SelectField(
        "Parent Page", description="The page this page is a child of, if any.",
        choices=[(0, "(toplevel)")] + [(page.id, page.title) for page in Pages.query.filter_by(parent_page_id=0).all()],
    )
    draft = BooleanField("Draft")
    hidden = BooleanField("Hidden")
    auth_required = BooleanField("Authentication Required")
    content = TextAreaField("Content")


class PageFilesUploadForm(BaseForm):
    file = MultipleFileField(
        "Upload Files",
        description="Attach multiple files using Control+Click or Cmd+Click.",
        validators=[InputRequired()],
    )
    type = HiddenField("Page Type", default="page", validators=[InputRequired()])
