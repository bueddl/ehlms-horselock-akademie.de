from wtforms import PasswordField, StringField
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired

from CTFd.forms import BaseForm
from CTFd.forms.fields import SubmitField
from CTFd.forms.users import attach_custom_user_fields, build_custom_user_fields


def RegistrationForm(*args, **kwargs):
    class _RegistrationForm(BaseForm):
        name = StringField("Deckname", validators=[InputRequired()])
        email = EmailField("E-Mail", validators=[InputRequired()])
        password = PasswordField("Codewort", validators=[InputRequired()])
        submit = SubmitField("Absenden")

        @property
        def extra(self):
            return build_custom_user_fields(
                self, include_entries=False, blacklisted_items=()
            )

    attach_custom_user_fields(_RegistrationForm)

    return _RegistrationForm(*args, **kwargs)


class LoginForm(BaseForm):
    name = StringField("Deckname oder E-Mail", validators=[InputRequired()])
    password = PasswordField("Codewort", validators=[InputRequired()])
    submit = SubmitField("Absenden")


class ConfirmForm(BaseForm):
    submit = SubmitField("Bestätigungs E-Mail noch einmal senden")


class ResetPasswordRequestForm(BaseForm):
    email = EmailField("E-Mail", validators=[InputRequired()])
    submit = SubmitField("Absenden")


class ResetPasswordForm(BaseForm):
    password = PasswordField("Codewort", validators=[InputRequired()])
    submit = SubmitField("Absenden")

